import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import('@/pages/Login'),
      name: 'Login',
      meta: {
        layout: 'minimal'
      }
    },
    {
      path: '/pipelines',
      component: () => import('@/pages/Pipelines'),
      name: 'Home'
    },
    {
      path: '/tracked-pipelines',
      component: () => import('@/pages/TrackedPipelines'),
      name: 'Tracked Pipelines'
    },
    {
      path: '/errors',
      component: () => import('@/pages/Errors'),
      name: 'Errors'
    },
    {
      path: '/telemetry',
      component: () => import('@/pages/Telemetry'),
      name: 'Telemetry'
    },
    {
      path: '*',
      component: () => import('@/pages/404'),
      name: '404',
      meta: {
        layout: 'minimal'
      }
    }
  ]
})
