// Font Awesome configuration

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Icons to import #treeshaking4theWin
import {
  faDungeon,
  faHistory,
  faPencilAlt,
  faPlay,
  faPlusSquare,
  faSkullCrossbones,
  faStop,
  faStream,
  faSyncAlt,
  faTimes,
  faTrash
} from '@fortawesome/free-solid-svg-icons'

// Add imported icons to the library
library.add(
  faDungeon,
  faHistory,
  faPencilAlt,
  faPlay,
  faPlusSquare,
  faSkullCrossbones,
  faStop,
  faStream,
  faSyncAlt,
  faTimes,
  faTrash
)

export default {
  install (Vue) {
    // Register the Vue Fontawesome component
    Vue.component('FontAwesomeIcon', FontAwesomeIcon)
  }
}
