// Configure Vue Navigation Bar

import VueNavigationBar from 'vue-navigation-bar'

import 'vue-navigation-bar/dist/vue-navigation-bar.css'

export default {
  install (Vue) {
    Vue.component('vue-navigation-bar', VueNavigationBar)
  }
}
