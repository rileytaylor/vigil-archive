// Globally install base vigil components

import ButtonBase from '@/components/ButtonBase'
import FormWrapper from '@/components/FormWrapper'
import InputBase from '@/components/InputBase'
import BannerBase from '@/components/BannerBase'
import CheckboxBase from '@/components/CheckboxBase'

export default {
  install (Vue) {
    Vue.component('ButtonBase', ButtonBase)
    Vue.component('FormWrapper', FormWrapper)
    Vue.component('InputBase', InputBase)
    Vue.component('BannerBase', BannerBase)
    Vue.component('CheckboxBase', CheckboxBase)
  }
}
