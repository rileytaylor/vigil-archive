// Configure Rollbar for Error Logging
import Rollbar from 'vue-rollbar'
import { version } from '../../package.json'

// Get environment for use by rollbar
const environment = process.env.NODE_ENV === 'production'
  ? 'production'
  : 'local'

const ROLLBAR_CLIENT_TOKEN = '4e88ad2917db4aebad4bd58b1cef7b17'

const rollbarConfig = {
  accessToken: ROLLBAR_CLIENT_TOKEN,
  captureUncaught: true,
  captureUnhandledRejections: true,
  enabled: true,
  environment: environment,
  payload: {
    client: {
      javascript: {
        code_version: version
        // source_map_enabled: true, TODO: upload sourcemaps during build: https://docs.rollbar.com/docs/vue-js
        // guess_uncaught_frames: true
      }
    }
  }
}

export default {
  install (Vue) {
    Vue.use(Rollbar, rollbarConfig)

    // Vue.config.productionTip = false
    Vue.config.errorHandler = function (err, vm, info) {
      Vue.rollbar.error(err)
    }
  }
}
