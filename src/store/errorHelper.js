/**
 * Forked from Special Handling and modified for this app Thanks Riley
 * Set errors into `state.errors` in a consistent manner, including handling catastrophic errors.
 * @param {Function} commit the action context.commit function to allow setting errors
 * @param {Object} err the error response object from the api request
 */
function handleErrors (commit, err) {
  // If it actually is an API error
  console.error(err)
  if (err.data) {
    if (err.data.statusCode === 400) {
      const errors = {}
      errors.body = err.data.body
      commit('SET_ERRORS', errors)
    } else {
      commit('SET_ERRORS', {
        server: `Uh oh! Server returned error status: ${err.data.statusCode} ${err.data.body}`
      })
    }
  }
}

export default handleErrors
