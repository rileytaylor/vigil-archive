const state = {
  user: null
}

const resetUser = {
  user: null
}

const mutations = {
  SET_USER (state, { ...fields }) {
    state.user = {
      ...fields
    }
  },
  CLEAR_USER (state) {
    state.user = {
      ...resetUser
    }
  }
}

const actions = {
  /**
  * Set a user as the `state.user`
  * @param {Object} context the context
  * @param {Object} payload a user object
  */
  setUser ({ commit }, payload) {
    commit('SET_USER', payload)
  },
  /**
  * Reset the `state.user`.
  * @param {Object} context the context
  */
  clearUser ({ commit }) {
    commit('CLEAR_USER')
  }
}

const getters = {
  user: state => state.user
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
