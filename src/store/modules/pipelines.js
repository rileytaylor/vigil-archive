import Vue from 'vue'
import TrackedPipelinesService from '../services/TrackedPipelinesService'
import StreamSetsService from '../services/StreamSetsService'
import handleErrors from '../errorHelper'

const state = {
  trackedPipelines: [],
  pipelines: [],
  errors: {}
}

const mutations = {
  ADD_PIPELINES (state, payload) {
    state.pipelines = payload
  },
  RESET_PIPELINES (state) {
    state.pipelines = []
  },
  ADD_TRACKED_PIPELINES (state, payload) {
    state.trackedPipelines = payload
  },
  RESET_TRACKED_PIPELINES (state) {
    state.trackedPipelines = []
  },
  ADD_TRACKED_PIPELINE (state, payload) {
    state.trackedPipelines.push(payload)
  },
  DELETE_TRACKED_PIPELINE (state, pipelineId) {
    let deletePipeIndex = state.trackedPipelines.findIndex(pipe => pipe.pipeline_id === pipelineId)
    if (deletePipeIndex !== -1) {
      state.trackedPipelines.splice(deletePipeIndex, 1)
    }
  },
  UPDATE_TRACKED_PIPELINE (state, payload) {
    let pipeIndex = state.trackedPipelines.findIndex(pipe => pipe.pipeline_id === payload.pipelineId)
    if (pipeIndex !== -1) {
      Vue.set(state.trackedPipelines, pipeIndex, payload.newTrackedPipeline)
    } else {
      console.error('Oh lawdy help us')
    }
  },
  SET_ERRORS (state, errors) {
    state.errors = errors
  },
  CLEAR_ERRORS (state) {
    state.errors = {}
  }
}

const actions = {
  /**
   * Fetch all pipelines from the api
   * @param {Object} context the context
   */
  async fetchPipelines ({ commit, dispatch }, reset) {
    await dispatch('fetchTrackedPipelines')
    // Parse out the JSON into objects and append those objects
    let payload = []
    let trackedPipelineNames = []
    state.trackedPipelines.forEach(pipe => trackedPipelineNames.push(pipe.pipeline_id))
    if (reset && reset === true) {
      commit('RESET_PIPELINES')
      try {
        await StreamSetsService.fetchAll().then(response => {
          let temp = JSON.parse(response.data.data)
          const arrConvert = Object.values(temp)
          arrConvert.forEach(pipe => {
            if (trackedPipelineNames.indexOf(pipe.name.slice(0, -36)) > -1) {
              payload.push(pipe)
            }
          })
        }).catch(function (error) {
          console.error(error)
        })
        commit('ADD_PIPELINES', payload)
      } catch (error) {
        console.error('Pipeline data cannot be retrieved ' + error.response.status + ' - ' + error.response.data)
      }
    } else {
      try {
        await StreamSetsService.fetchAll().then(response => {
          let temp = JSON.parse(response.data.data)
          const arrConvert = Object.values(temp)
          arrConvert.forEach(pipe => {
            if (trackedPipelineNames.indexOf(pipe.name.slice(0, -36)) > -1) {
              payload.push(pipe)
            }
          })
        }).catch(function (error) {
          console.error(error)
        })
        commit('ADD_PIPELINES', payload)
      } catch (error) {
        console.error('Pipeline data cannot be retrieved ' + error.response.status + ' - ' + error.response.data)
      }
    }
  },
  /**
   * Reset `state.pipelines`
   * @param {Object} context the context
   */
  clearPipelines ({ commit }) {
    commit('RESET_PIPELINES')
  },
  /**
   * Clear errors from `state.errors`
   * @param {Object} context the context
   */
  clearErrors ({ commit }) {
    commit('CLEAR_ERRORS')
  },
  /**
   * Fetch all trackedPipelines from the api
   * @param {Object} context the context
   */
  async fetchTrackedPipelines ({ commit }) {
    try {
      const responsePipes = await TrackedPipelinesService.fetchAll()
      commit('ADD_TRACKED_PIPELINES', responsePipes.data)
    } catch (error) {
      console.error('Could not retrieve tracked pipelines ' + error.response.status + ' - ' + error.response.data)
    }
  },
  async addTrackedPipeline ({ commit }, payload) {
    try {
      const response = await TrackedPipelinesService.track(payload)
      if (response.data.statusCode === 201) {
        commit('ADD_TRACKED_PIPELINE', payload)
      } else {
        handleErrors(commit, response)
      }
    } catch (error) {
      console.error('Could not track pipeline' + error.response.status + ' - ' + error.response.data)
    }
  },
  async deleteTrackedPipeline ({ commit }, pipelineId) {
    try {
      const response = await TrackedPipelinesService.untrack(pipelineId)
      commit('CLEAR_ERRORS')
      if (response.data.statusCode === 202) {
        commit('DELETE_TRACKED_PIPELINE', pipelineId)
        return response.data.statusCode
      } else {
        handleErrors(commit, response)
        return response.data.statusCode
      }
    } catch (error) {
      console.error('Could not untrack pipeline' + error.response.status + ' - ' + error.response.data)
    }
  },
  async updateTrackedPipeline ({ commit }, payload) {
    try {
      const response = await TrackedPipelinesService.edit(payload.pipeline_id, payload.payload)
      commit('CLEAR_ERRORS')
      if (response.data.statusCode === 200) {
        const passMeIn = {
          'pipelineId': payload.pipeline_id,
          'newTrackedPipeline': payload.payload
        }
        commit('UPDATE_TRACKED_PIPELINE', passMeIn)
        return response.data.statusCode
      } else {
        handleErrors(commit, response)
        return response.data.statusCode
      }
    } catch (error) {
      console.error('Could not update pipeline' + error.response.status + ' - ' + error.response.data)
    }
  }
}

const getters = {
  trackedPipelines: state => state.trackedPipelines,
  trackedStreamingPipelines: state => {
    return state.trackedPipelines.filter(pipe => pipe.streaming)
  },
  trackedBatchPipelines: state => {
    return state.trackedPipelines.filter(pipe => pipe.batch)
  },
  pipelines: state => state.pipelines,
  errors: state => state.errors
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
