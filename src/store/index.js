import Vue from 'vue'
import Vuex from 'vuex'

import users from '@/store/modules/users'
import pipelines from '@/store/modules/pipelines'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    users,
    pipelines
  }
})

export default store
