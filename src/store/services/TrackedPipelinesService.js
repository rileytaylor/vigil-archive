/**
 * Methods for CRUD operations around tracked-pipelines
*/

import axios from 'axios'

export default {
  /**
   * Return a list of tracked pipelines
   */
  fetchAll () {
    return axios.get(`${process.env.VUE_APP_LAMBDA_URL}/tracked-pipelines`, {
      headers: {
        'x-api-key': process.env.VUE_APP_LAMBDA_API_KEY
      }
    })
  },

  /**
   * Return a single tracked pipeline
   * @param {String} pipelineId the pipeline id
   */
  fetchOne (pipelineId) {
    return axios.get(`${process.env.VUE_APP_LAMBDA_URL}/tracked-pipelines/${pipelineId}`, {
      headers: {
        'x-api-key': process.env.VUE_APP_LAMBDA_API_KEY
      }
    })
  },

  /**
   * track a new pipeline
   * @param {Object} payload the pipeline payload
   */
  track (payload) {
    const options = {
      headers: {
        'x-api-key': process.env.VUE_APP_LAMBDA_API_KEY,
        'Content-Type': 'application/json'
      }
    }
    return axios.post(`${process.env.VUE_APP_LAMBDA_URL}/tracked-pipelines`, {
      id: 0,
      added_by: payload['added_by'],
      pipeline_id: payload['pipeline_id'],
      batch: payload['batch'],
      streaming: payload['streaming']
    }, options)
  },
  /**
   * Edits a single tracked pipeline based on pipeline id
   * @param {String} pipelineId the pipeline id
   * @param {Object} payload the pipeline payload
   */
  edit (pipelineId, payload) {
    const options = {
      headers: {
        'x-api-key': process.env.VUE_APP_LAMBDA_API_KEY,
        'Content-Type': 'application/json'
      }
    }
    return axios.put(`${process.env.VUE_APP_LAMBDA_URL}/tracked-pipelines/${pipelineId}`, {
      id: 0,
      added_by: payload['added_by'],
      pipeline_id: payload['pipeline_id'],
      batch: payload['batch'],
      streaming: payload['streaming']
    }, options)
  },

  /**
   * untracks a pipeline based on provided pipeline id
   * @param {String} pipelineId the pipeline id
   */
  untrack (pipelineId) {
    return axios.delete(`${process.env.VUE_APP_LAMBDA_URL}/tracked-pipelines/${pipelineId}`, {
      headers: {
        'x-api-key': process.env.VUE_APP_LAMBDA_API_KEY
      }
    })
  }
}
