// Library imports
import Vue from 'vue'
import VModal from 'vue-js-modal'
import VueUp from 'vueup'
import VueGoodTablePlugin from 'vue-good-table'
import VueShortkey from 'vue-shortkey'

// Project imports
import App from '@/App'
import router from '@/router'
import store from '@/store'
import FontAwesomePlugin from '@/utils/fontawesome'
import RollbarPlugin from '@/utils/rollbar'
import VueNavigationBarPlugin from '@/utils/vueNavigationBar'
import VigilComponents from '@/utils/vigilComponents'

// Style imports
import '@/styles.scss'

// Layouts
import DefaultLayout from '@/layouts/Default'
import MinimalLayout from '@/layouts/Minimal'

// Register layouts
Vue.component('default-layout', DefaultLayout)
Vue.component('minimal-layout', MinimalLayout)

// Use things
Vue.use(VModal)
Vue.use(VueUp)
Vue.use(VueGoodTablePlugin)
Vue.use(VueNavigationBarPlugin)
Vue.use(RollbarPlugin)
Vue.use(FontAwesomePlugin)
Vue.use(VigilComponents)
Vue.use(VueShortkey)

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
