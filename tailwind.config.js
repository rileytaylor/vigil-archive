// tailwind.config.js
const { colors, fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
      boxShadow: {
        'outline-primary': '0 0 0 3px rgba(56,178,172,0.5)'
      },
      colors: {
        primary: colors.teal,
        secondary: colors.gray,
        danger: colors.red,
        success: colors.green,
        warning: colors.yellow
      },
      fontFamily: {
        sans: ["'Oxygen'", ...fontFamily.sans]
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms')
  ]
}
