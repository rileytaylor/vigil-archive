<div style="text-align:center">

# Vigil

![logo](./src/assets/VigilFull.png)

A monitoring application for StreamSets pipelines.

</div>

Vue.js + Vue Router + Vuex + Express + Tailwind.css + Docker

## Getting started

Configure enviornment variables by copying `.env.local.example` to `.env.local` and configuring there.

```sh
# Copy .env file, then fill it out
cp .env.local.example .env.local
# Copy server/.env file, then fill it out
cp server/.env.example server/.env

# Install dependencies
npm install

# Build frontend
npm run build:dev

# In another terminal process
npm run serve:backend
```

## Tooling

### Commands

- `npm run build:dev` -> Build the frontend for development with HMR
- `npm run build:prod` -> Build the frontend for production
- `npm run lint` -> Lint things
- `npm run serve:backend` -> Serve the application with the auth stack enabled
- `npm run serve:vue` -> Run the Vue.js local server, good for utilizing vue-cli's built in analysis tools and circumventing the auth stack if necessary

### Docker Stuff

To build the container run `docker build -t my-app:prod .`

To start the container run `docker run -it -p 3000:3000 --rm my-app:prod`

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Error Monitoring

This project uses Rollbar to monitor errors. Anything that makes its way to the global error handler will be sent to Rollbar (i.e. anything that is unhandled).

On the Vue.js frontend, you can also log errors directly to rollbar via the `this.$rollbar` global method. If you use a try/catch and want to send information to rollbar depending on how its handled, you'll need to do this manually.

```js
this.$rollbar.info('Logging some info')
this.$rollbar.error(error) // Takes in an error from a catch statement
```

You can log errors directly on the express backend as well:

```js
rollbar.log('info')
rollbar.error(error) // An error from a catch statement
```

## Application Structure

Well there's a frontend and a backend so here ya go.

### Frontend

The frontend lives inside of the `src/` folder.

- **assets/**:
- **components/**: Components are components. For information on how they are named, used, and structured in this app see [here](#components).
- **layouts/**: Layouts are components that wrap pages with necessary scaffolding, such as a navbar, and are loaded automatically by the router. These behave similarly to Nuxt.js layouts. [More information here](#layouts)
- **pages/**: Pages are components used for the main content of each route.
- **router/**: vue-router config
- **store/**: vuex config
- **utils/**: various utilities, including plugins to simplify application init logic. For information about each plugin, see each file.
- **App.vue**: The root vue component
- **main.js**: The main entry point for the application where everything is inited
- **styles.scss**: Base styles for the application, including font loading, tailwind loading, and more

#### Components

Generally there will be two types of components: `Base` components that serve as Vigil's basic UI component library and know nothing specific about the application itself, and everything else, which are specific components for breaking out UX.

For example: `InputBase` is a basic UI component, while `PipelineCard` is specific to the Vigil application and knows about its data structure.

The idea here is that any `Base` component _could_ be broken out into a seperate library and has no specific knowledge about this application.

**Global Registration:** `Base` components can be globally registered for ease of use, since they are meant to act like a component library. To add it to global imports, follow the pattern [in the Vigil Components Plugin here](src/utils/vigilComponents.js).

Other components should be imported directly into other components and not globally registered.

#### Layouts

Layouts are simple wrapper components for pages. There is a [default layout](src/layouts/Default.vue) (`Default`) which is used unless another layout is specified via a `meta` tag in the router config, like so:

```js
{
  path: '/',
  component: () => import('@/pages/Login'),
  name: 'Login',
  meta: {
    layout: 'minimal'
  }
}
```

Layouts should be as simple as possible, generally only including headers, navigation, footers, or content re-used in many pages.

### Backend

TODO
- **server/**: The backend part of Vigil that is included in this repo is very simple. It has one job, to authenticate users. It does not even create users just authenticates them. To do that we use Express as our Node Server of choice and the following packages imported in the following way.
```
const path = require('path')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const passport = require('passport')
const mysql = require('mysql2')
const bcrypt = require('bcrypt')
const history = require('connect-history-api-fallback')
```
To persist these accounts safely we are using a table in Memento called `vigil_auth` usernames, emails, and salted + hashed passwords are stored in that table. The actual authentication strategy is JWTs managed through Passport. Not all pages in Vigil require authentication. If by some miracle you are reading this and need access to a page of the application that is protected please reach out the Integration team attached to the Trellis project.
