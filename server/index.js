'use strict'

const express = require('express')

const app = express()

const path = require('path')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const passport = require('passport')
const mysql = require('mysql2')
const bcrypt = require('bcrypt')
const history = require('connect-history-api-fallback')
const Rollbar = require('rollbar')

require('dotenv').config({ path: path.resolve(__dirname, '.env') })

const environment = process.env.NODE_ENV === 'production'
  ? 'production'
  : 'development'

// Configure rollbar
// This will automatically catch any unhandled errors. To log manually,
// use `rollbar.log('level', 'message')` or `rollbar.error(e)` with your exception.
// See docs for more info: https://docs.rollbar.com/docs/nodejs
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_SERVER_TOKEN,
  captureUncaught: true,
  captureUnhandledRejections: true,
  enabled: true,
  environment: environment
})

// Set up rollbar
app.use(rollbar.errorHandler())

// const staticFileMiddleware = express.static('../dist')
const staticFileMiddleware = express.static(path.resolve(__dirname, '../dist'))

app.use(staticFileMiddleware)

// getting the local authentication type
const LocalStrategy = require('passport-local').Strategy

// Proxy our vue-router calls
app.use(history({
  disableDotRule: true,
  index: '/index.html'
}))

app.use(bodyParser.json())
app.use(cookieSession({
  name: 'mysession',
  keys: [process.env.VUE_AUTH_KEY],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

app.use(passport.initialize())
app.use(passport.session())

const pool = mysql.createPool({
  host: process.env.MEMENTO_HOST,
  user: process.env.MEMENTO_USERNAME,
  password: process.env.MEMENTO_PASSWORD,
  database: 'memento',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

const promisePool = pool.promise()

async function retrieveUserEmail (value) {
  try {
    const result = await promisePool.query('SELECT * FROM vigil_auth WHERE email = ?', [
      value
    ])
    let prunedResult = Object.assign({}, result[0][0])
    return prunedResult
  } catch (e) {
    console.error(e)
  }
}

async function retrieveUserId (value) {
  try {
    const result = await promisePool.query('SELECT * FROM vigil_auth WHERE id = ?', [
      value
    ])
    let prunedResult = Object.assign({}, result[0][0])
    return prunedResult
  } catch (e) {
    console.error(e)
  }
}

// This has to be done twice for some reason
app.use(staticFileMiddleware)

app.post('/api/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err)
    } if (!user) {
      return res.status(400).send([user, 'Cannot log in', info])
    }
    req.login(user, (err) => {
      res.send('Logged in')
      if (err) {
        console.error(err)
      }
    })
  })(req, res, next)
})

app.get('/api/logout', function (req, res) {
  req.logout()
  return res.send()
})

const authMiddleware = (req, res, next) => {
  if (!req.isAuthenticated()) {
    res.status(401).send('You are not authenticated')
  } else {
    return next()
  }
}

app.get('/api/user', authMiddleware, async (req, res) => {
  let user = await retrieveUserId(req.session.passport.user)
  res.send({
    user: user
  })
})

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, async (username, password, done) => {
  try {
    let user = await retrieveUserEmail(username)
    if (user) {
      let hashed = null
      bcrypt.hash(password, user.salt).then(hash => {
        hashed = hash
      }).then(() => {
        if (user.password === hashed) {
          done(null, user)
        } else {
          done(null, false, {
            message: 'Incorrect username or password'
          })
        }
      })
    } else {
      done(null, false, {
        message: 'Incorrect username or password'
      })
    }
  } catch (e) {
    return done(e)
  }
}
))

passport.serializeUser(async (user, done) => {
  done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
  let user = await retrieveUserId(id)
  done(null, user)
})

app.listen(3000, () => {
  console.log('app listening on port 3000')
})
