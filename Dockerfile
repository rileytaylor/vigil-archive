# build environment
FROM node:12.2.0-alpine as build
# Do this so stuff installs who knows
RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers autoconf automake make nasm python git && \
  npm install --quiet node-gyp -g
WORKDIR /
ENV PATH /node_modules/.bin:$PATH
COPY package.json /package.json
RUN npm install --silent
RUN npm install @vue/cli@3.7.0 -g
COPY . /
RUN npm run build:prod

# production environment
FROM node:12.2.0-alpine
COPY --from=build / /
EXPOSE 3000
ENV NODE_ENV production
CMD ["npm", "run", "serve:backend"]
